const chai = require("chai");
const {assert} = require("chai");

const http = require("chai-http");
chai.use(http);

describe("api_test_suite_users", () => {
	it("test_api_get_users_is_running", () => {
		chai.request("http://localhost:4000")
			.get("/users")
			.end((err, res) => {
				assert.isDefined(res);
			})
	});

	it("text_api_get_users_returns_array", () => {
		chai.request("http://localhost:4000")
			.get("/users")
			.end((err, res) => {
				// console.log(res.body[1]);
				assert.isArray(res.body);
		})
	});

	it("text_api_get_users_first_item_object_is_john", () => {
		chai.request("http://localhost:4000")
			.get("/users")
			.end((err, res) => {
				// console.log(res.body[0]);
				assert.equal(res.body[0].name, "John");
				

		})
	});

	it("text_api_get_users_first_item_is_Smith", () => {
		chai.request("http://localhost:4000")
			.get("/users")
			.end((err, res) => {
				// console.log(res.body.length - 1);
				assert.notEqual(res.body[res.body.length - 1], undefined);
			})
	});

	it("test_api_post_users_returns_4000_if_no_name", (done) => {
		//.post() which is used by chai http to access a post method route
		//.type() which is used to tell chai that the request body is going to be stringified as json
		//.send() is used to send the request body
		chai.request("http://localhost:4000")
		.post("/users")
		.type("json")
		.send({
			age: 31,
			username: "irene91"
		})
		.end((err,res)=>{

			assert.equal(res.status,400)
			done();
		})

	})

});

describe("api_test_suite_users", () => {
	it("test_api_get_products_if_running", (done) => {
		chai.request("http://localhost:4000")
			.get("/products")
			.end((err, res) => {
				assert.isDefined(res);
				done();
			});
	});

	it("test_api_products_returns_array", (done) => {
		chai.request("http://localhost:4000")
			.get("/products")
			.end((err, res) => {
				assert.isArray(res.body);
				done();
		});
	});

	it("test_api_products_first_item_is_object", (done) => {
		chai.request("http://localhost:4000")
			.get("/products")
			.end((err, res) => {
				assert.isNotObject(res.body[0]);
				done();
			})
	});
});


describe("api_test_suite_post_user", () => {
	it("api_test_if_running", (done) => {
		chai.request("http://localhost:4000")
			.post("/users")
			.type("json")
			.send({
				age:31,
				name: "james"
			})
			.end((req, res) => {
				assert.isDefined(res);
				done();
			})
	})
	it("test_api_post_if_theres_no_username", (done) => {
		chai.request("http://localhost:4000")
			.post("/users")
			.type("json")
			.send({
				age: 31,
				name: "james"
			})
			.end((req, res) => {
				assert.equal(res.status, 400);
				done();
			})
	})

	it("test_api_post_if_theres_no_age_property", (done) => {
		chai.request("http://localhost:4000")
			.post("/users")
			.type("json")
			.send({
				name: "haste",
				username: "Boom boom"
			})
			.end((req, res) => {
				assert.equal(res.status, 400);
				done();
			})
	});
})