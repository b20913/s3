const express = require("express");
const app = express();
app.use(express.json());

let users = [
{
	"name": "John",
	"age": 18,
	"username": "johnsmith99"
},
{
	"name": "Johnson",
	"age": 21,
	"username": "johnson199"
},
{
	"name": "Smith",
	"age": 19,
	"username": "smithMike12"
}
];

let products = [
{
	name: "Vageline",
	price: "$19.50",
	isActive: true
},
{
	name: "Panlen",
	price: "$23.00",
	isActive: false
},
{
	name: "Testmusk",
	price: "$59.99",
	isActive: true
}
]

app.get("/users", (req, res) => {
	return res.send(users);
})

app.get("/products", (req, res) => {
	return res.send(products);
});

app.post("/users", (req, res) => {
	// add a simple if state that if the request body diest bit gave a name property
	if(!req.body.hasOwnProperty("name")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter NAME"
		})
	}

	if(!req.body.hasOwnProperty("age")){
		return res.status(400).send({

			error: "Bad Request - missing required parameter AGE"

		})
	}

	if(!req.body.hasOwnProperty("username")){
		return res.status(400).send({

			error: "Bad Request - missing required parameter username"

		})
	}

})

// const start = async () => {
// 	try {
// 		app.listen(4000, () => {
// 			console.log(`server is running`);
// 		});
// 	} catch(error) {
// 		console.log(error);
// 	}
// }

app.listen(4000, () => {
	console.log("serviser running at port 4000")
})